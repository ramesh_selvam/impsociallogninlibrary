import XCTest

import IMPSocialSigninLibraryTests

var tests = [XCTestCaseEntry]()
tests += IMPSocialSigninLibraryTests.allTests()
XCTMain(tests)
